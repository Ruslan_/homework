<?php

class MyDataBase
{
    const IMPRESSIONS = 6;

    protected static function dbParams()
    {
        return array(
            'host' => 'localhost',
            'dbname' => 'home_work',
            'user' => 'root',
            'password' => ''
        );
    }

    public static function getConnection()
    {

        $db_connect = new PDO('mysql:host='.self::dbParams()['host'].';
        dbname='.self::dbParams()['dbname'], self::dbParams()['user'], self::dbParams()['password']);

        return $db_connect;

    }
}