-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 31 2018 г., 00:11
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `home_work`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Дніпропетровське підприємство «Хутірське» вирощуватиме кукурудзу під зрошенням', 'Агропідприємтво «Хутірське» (Дніпропетровська область) облаштувало зрошувальну систему. \r\n\r\nДля цього придбано дощувальну машину фронтальної дії компанії «Варіант Агро Буд».\r\n\r\nОбладнання запущено для поливу поля кукурудзи довжиною 2 тис. м.\r\n\r\nПродуктивність системи становить 200 куб. м/год. Поливна площа обладнання – 74 га. ', '2018-05-24 16:06:24', NULL),
(2, '«Лучше бегите сами», — мэр Днепра пригрозил «наливайкам» судьбой металлистов', 'В Днепре продолжается снос незаконных МАФов, в которых «предприниматели» без разрешения разливают суррогатный алкоголь. Мэр Днепра Борис Филатов пригрозил владельцам «наливаек» и предложил бежать из города. Об этом журналист «НМ» узнал на странице Facebook городского головы города.\r\n«Я же предупреждал. Предупреждал же, правда? С сегодняшнего дня начали массово крушить наливайки. Лучше бегите сами. А не ждите судьбы металлоломщиков», — пригрозил Борис Филатов.', '2018-05-24 16:06:24', NULL),
(22, 'new ', 'text text', '2018-05-26 10:33:54', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `publish` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `publish`) VALUES
(1, 'Женщинам', 'Товары для женщин от лучших брендов купить в - интернет-магазин. Постоянные акции на женские товары, скидки, распродажи!', 1),
(2, 'Мужчинам', 'Товары для мужчин от лучших брендов купить в - интернет-магазин. Постоянные акции на мужские товары, скидки, распродажи!', 1),
(3, 'Детям', 'Всё для детей в интернет-магазине! Мы всегда рады помочь Вам выбрать лучшее!', 1),
(4, 'Животным', 'Товары для животных с быстрой доставкой на дом можно купить в интернет зоомагазине', 1),
(5, 'Аксессуары', 'Продажа аксессуаров — купить мужские или женские аксессуары в сервисе объявлений по Украине. Покупай модные аксессуары', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `orders_to_products`
--

CREATE TABLE `orders_to_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `articul` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `publish` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `articul`, `brand`, `image_path`, `description`, `price`, `category_id`, `created_at`, `updated_at`, `publish`) VALUES
(1, 'Кроссовки Adidas Originals ClimaCool', 765, 'Adidas', '', 'Adidas – немецкий промышленный концерн, специализирующийся на выпуске спортивной обуви, одежды, сумок, часов, очков, инвентаря и других товаров, связанных со спортом. Компания является крупнейшим производителем спортивных товаров в Европе и вторым по величине в мире.', '3990.00', 2, '2018-05-30 17:59:50', NULL, 1),
(2, 'Кроссовки Puma Axis V3 Mesh', 732, 'Puma', '', 'Неизвестно, насколько обширными были амбиции герра Рудольфа Дасслера, когда в 1948-м, он открывал компанию по производству спортивной обуви. Изначальное название Ruda, по первым слогам его имени и фамилии, вскоре было заменено на более благозвучное Puma, принеся с собой и ассоциации с быстрым и ловким зверем.', '1599.00', 2, '2018-05-30 17:59:50', NULL, 1),
(3, 'Кроссовки Nike Air MAX GUILE', 744, 'Nike', '', 'Nike – один из самых гигантских производителей спортивной обуви и одежды, также аксессуаров и снаряжения по всему миру. Залог успеха американского бренда очень простой: фирма разрабатывает новейшие революционные технологии и внимательно следит за качеством своей продукции. Это оптимальный выбор для тех, кто обожает спортивный стиль.', '3999.00', 2, '2018-05-30 18:03:18', NULL, 1),
(4, 'Футболка Adidas Athletic Vibe M', 865, 'Adidas', '', 'Adidas (Адидас) – немецкий промышленный концерн, специализирующийся на выпуске спортивной обуви, одежды, сумок, часов, очков, инвентаря и других товаров, связанных со спортом. Компания является крупнейшим производителем спортивных товаров в Европе и вторым по величине в мире. Бренд adidas сегодня является одним из самых известных и продаваемых в мире спортивной экипировки. Сегодня он ассоциируется не только с обувью. Бренд пользуется большой любовью у молодежи и профессиональных спортсменов и имеет прочные позиции в футбольном мире.', '1090.00', 2, '2018-05-30 18:08:25', NULL, 1),
(5, 'Футболка Nike M Nk Miler Top Ss', 821, 'Nike', '', 'ike – один из самых гигантских производителей спортивной обуви, одежды, аксессуаров и снаряжения. Секрет успеха американского бренда звучит просто: фирма неустанно изобретает новейшие, революционные технологии и пристально отслеживает качество своей продукции. Это оптимальный выбор для тех, кто обожает спортивный стиль.', '899.00', 2, '2018-05-30 18:08:25', NULL, 1),
(6, 'Джинсы Mango 23013601-TO', 1234, 'Mango', '', 'Mango — это крупнейшая компания по производству модной одежды, обуви, сумок и аксессуаров для женщин и мужчин.\r\n\r\nMango является одним из самых известных брендов женской моды в средней ценовой категории, предлагающий все самые актуальные и интересные тренды современной одежды, обуви, украшений и сумочек в своих довольно доступных по ценам коллекциях', '1259.00', 1, '2018-05-30 18:12:08', NULL, 1),
(7, 'Джинсы H&M 0179446 40', 1233, 'H&M', '', 'Hennes & Mauritz (H&M) — известный шведский бренд, который производит одежду, обувь, нижнее белье, косметику, аксессуары в невысоком ценовом сегменте. Над коллекциями бренда H&M работают креативные дизайнеры, а в производстве используются современные технологии. Сегодня H&M — это самая крупная сеть в Европе. Ее покупатели – это молодые люди, которые хотят недорого и стильно одеваться, выглядеть на все 100% и с каждой новой коллекцией менять свой образ.', '349.00', 1, '2018-05-30 18:12:08', NULL, 1),
(8, 'Ветровка Nike Tech Hyper Mesh Bomber', 1232, 'Nike', '', 'Комфортная ветровка станет великолепным решением для домашней коллекции. Товар сделан из качественных материалов приятного окраса. Внешний вид выверен до мелочей. Все составляющие тщательно подобраны и удачно сочетаются друг с другом. Данный товар будет замечательным приобретением или подарком на любой праздник. Ветровка прекрасно подойдет под ваш имидж и выставит вас в выгодном свете и в офисе и в магазине.', '1849.00', 1, '2018-05-30 18:16:09', NULL, 1),
(9, 'Куртка H&M 0479632 34 Темно-зеленая', 1231, 'H&M', '', 'Hennes & Mauritz (H&M) — известный шведский бренд, который производит одежду, обувь, нижнее белье, косметику, аксессуары в невысоком ценовом сегменте. Над коллекциями бренда H&M работают креативные дизайнеры, а в производстве используются современные технологии. Сегодня H&M — это самая крупная сеть в Европе. Ее покупатели – это молодые люди, которые хотят недорого и стильно одеваться, выглядеть на все 100% и с каждой новой коллекцией менять свой образ.', '799.00', 1, '2018-05-30 18:16:09', NULL, 1),
(10, 'Юбка Zironka 30-7002-1 98 см', 2232, 'Zironka ', '', 'Zironka — этот украинский производитель в течение многих лет отличается хорошим качеством своей продукции и отличными ценами. Производство одежды для детей от 1 года до подросткового возраста – это ответственная миссия. Ведь этот период очень важен для дальнейшей жизни уже взрослого человека. Zironka для детей производит только лучшую одежду. С заботой о будущем поколении, команда профессионалов из года в год выпускает качественные и стильные коллекции одежды. Для детей модельеры создают модную повседневную одежду, платья для девочек и костюмы для мальчиков, конверты на выписку в подарочных упаковках, конверты для новорожденных, школьную форму и многое другое.', '195.00', 3, '2018-05-30 18:21:27', NULL, 1),
(11, 'Платье Кена 110101-03 92 см Розовое', 2231, 'Кена', '', 'Торговая марка Кена - это востребованный украинский производитель. Кена поставляет качественную одежду для детей от ноля до восьми лет. Занимаясь производством исключительно одежды для самых маленьких, Кена ответственно подходит к вопросу качества. Маленькие детки нуждаются в заботе об их нежной и бархатной коже, учитывая эти и другие особенности столь специфичного потребителя, Кена использует только самые лучшие полотна и ткани. Компания сразу же зарекомендовала себя как производитель качественной продукции, доступной по цене.', '104.00', 3, '2018-05-30 18:21:27', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '096-655-44-33', '123', '2018-05-29 21:22:17', NULL),
(2, 'moderator', 'moderator@gmail.com', '050-321-21-23', '321', '2018-05-29 21:22:17', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `phone` (`phone`);

--
-- Индексы таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `price` (`price`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `price` (`price`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`phone`) REFERENCES `users` (`phone`),
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  ADD CONSTRAINT `orders_to_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  ADD CONSTRAINT `orders_to_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `orders_to_products_ibfk_3` FOREIGN KEY (`price`) REFERENCES `products` (`price`);

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
