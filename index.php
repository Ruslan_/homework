<?php
session_start();
define('ROOT', __DIR__);
require_once ROOT.'/components/MyDataBase.php';
require_once ROOT.'/models/Products.php';
require_once ROOT.'/models/Articles.php';
require_once ROOT.'/models/Categories.php';

$categories = new Categories();
$products = new Products();
$db = MyDataBase::getConnection();
$categoriesConnection = $categories->select($db);
$rowCategories = $categoriesConnection->fetchAll(PDO::FETCH_ASSOC);



require_once ROOT.'/view/template-html/includes/head.php';
require_once ROOT.'/view/template-html/includes/header.php';

require_once ROOT.'/view/template-html/main.php';

require_once ROOT.'/view/template-html/includes/footer.php';