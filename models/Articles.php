<?php


class Articles
{
    /**
 * @param $id пременная типа int
 * @param $delete соединение с базой двнных
 * @param $table название таблицы
 */
    public function delete($id, $delete)
    {
        $var = $delete->prepare('DELETE FROM  articles WHERE id=:id');
        $var->bindValue(':id', $id, PDO::PARAM_INT);
        $var->execute();
    }

    /**
     * @param $id пременная типа int
     * @param $select соединение с базой двнных
     */
    public function select($id, $select)
    {
        $var = $select->prepare('SELECT id, title, description, created_at FROM articles ORDER BY :id ASC ');
        $var->bindValue(':id', $id, PDO::PARAM_INT);
        $var->execute();


    }

    /**
     * @param $title название статьи string
     * @param $description описание статьи string
     * @param $insert соединение с базой двнных
     */
    public function insert($title, $description, $insert)
    {
        $var = $insert->prepare('INSERT INTO articles (title, description)
                    VALUES (:title, :description)');
        $var->bindValue(':title', $title, PDO::PARAM_STR);
        $var->bindValue(':description', $description, PDO::PARAM_STR);
        $var->execute();

    }

    /**
     * @param $title название статьи string
     * @param $description описание статьи string
     * @param $id пременная типа int
     * @param $update соединение с базой двнных
     */
    public function update($title, $description, $id, $update)
    {
        $var=$update->prepare('UPDATA articles SET title=:title description=:description WHER id=:id');
        $var->bindValue(':title', $title, PDO::PARAM_STR);
        $var->bindValue(':description', $description, PDO::PARAM_STR);
        $var->bindValue(':id', $id, PDO::PARAM_INT);
        $var->execute();
    }
}