<?php

class Categories
{
    public function select($dbConnect)
    {
        $var=$dbConnect->query('SELECT id, name, description, publish FROM categories ORDER BY id ASC');
        return $var;
    }
}