<?php


class Products
{
//    public $id;
//    public $name;
//    public $brand;
//    public $image_path;
//    public $description;
//    public $price;
//    public $category_id;
//    public $updated_at;
//    public $publish;
//    public $dbConnect;
//    public $task;


    public function select($dbConnect, $task=null, $priceFrom, $priceBefore, $sort=null)
    {
        if ($task == null){
            $var = $dbConnect->prepare(
                'SELECT id, name, brand, image_path, description, price, publish
            FROM products ORDER BY name ASC ');
        }elseif($task == 'price'){
        $var = $dbConnect->prepare(
            'SELECT id, name, brand, image_path, description, price, publish
            FROM products WHERE price >= :priceFrom AND price <= :priceBefore ORDER BY price DESC ');
        $var->bindValue(':pricepriceFrom', $priceFrom, PDO::PARAM_STR);
        $var->bindValue(':priceBefore', $priceBefore, PDO::PARAM_STR);
        }elseif ($task =='sort' && $sort == 'timeDESC'){
            $var = $dbConnect->prepare(
                'SELECT id, name, brand, image_path, description, price, publish
            FROM products ORDER BY id DESC ');
        }elseif ($task =='sort' && $sort == 'timeASC'){
            $var = $dbConnect->prepare(
                'SELECT id, name, brand, image_path, description, price, publish
            FROM products ORDER BY id ASC ');
        }elseif ($task =='sort' && $sort == 'nameASC'){
            $var = $dbConnect->prepare(
                'SELECT id, name, brand, image_path, description, price, publish
            FROM products ORDER BY name ASC ');
        }elseif ($task =='sort' && $sort == 'nameDESC'){
            $var = $dbConnect->prepare(
                'SELECT id, name, brand, image_path, description, price, publish
            FROM products ORDER BY name DESC ');
        }

        $var->execute();
    }

    public function delete($id, $dbConnect)
    {
        $var = $dbConnect->prepare('DELETE FROM  articles WHERE id=:id');
        $var->bindValue(':id', $id, PDO::PARAM_INT);
        $var->execute();
    }


}