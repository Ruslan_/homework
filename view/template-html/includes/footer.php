<footer id="footer"><!--Footer-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © <?=date('Y')?></p>
                <p class="pull-right">Pomazan Ruslan</p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->



<script src="../view/template-html/assets/js/jquery.js"></script>
<script src="../view/template-html/assets/js/bootstrap.min.js"></script>
<script src="../view/template-html/assets/js/jquery.scrollUp.min.js"></script>
<script src="../view/template-html/assets/js/price-range.js"></script>
<script src="../view/template-html/assets/js/jquery.prettyPhoto.js"></script>
<script src="../view/template-html/assets/js/main.js"></script>
</body>
</html>