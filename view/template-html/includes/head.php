
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link href="../view/template-html/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/prettyPhoto.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/price-range.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/animate.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/main.css" rel="stylesheet">
        <link href="../view/template-html/assets/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="../view/template-html/assets/js/html5shiv.js"></script>
        <script src="../view/template-html/assets/js/respond.min.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../view/template-html/assets/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../view/template-html/assets/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../view/template-html/assets/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../view/template-html/assets/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->